<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        //Inicializando variable y asignandole un array de numeros
        $v5=array(0,1,0,1,0);
        
        //recorriendo el array contenido en la variable con un foreach        
        foreach($v5 as $key=>$value){
            echo "<br>\$v5[$key]=$value";
        }
        
        //recorriendo el array contenido en la variable con un for    
        for($c=0;$c<count($v5);$c++){
            echo "<br>\$v5[$c]=$v5[$c]";
        }
        ?>
    </body>
</html>
