<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        //inicializando variables y asignandoles un valor
        $alumnos=array();
        $alumnos[]="Ramon";
        $alumnos[]="Jose";
        $alumnos[]="Pepe";
        $alumnos[]="Ana";
        
        //inicializando variable y asignandole el valor de array
        $alumnos1=array("Ramon","Jose","Pepe","Ana");
        
        //recorriendo el array de alumnos para mostrar los nombres que contiene
        for($c=0;$c<count($alumnos);$c++){
            echo "$alumnos[$c]<br>";
        }
        
        //recorriendo el array de la variable alumnos1 y asignando ese valor a la variable value, que despues de muestra
        foreach($alumnos1 as $value){
            echo "$value<br>";
        }
        ?>
    </body>
</html>
