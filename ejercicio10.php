<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        //Inicializando variables y asignandoles valor
        $cadena1="Ramon Abramo";
        $cadena2="ramon@alpeformacion.es";
        $cadena=$cadena1 . " - " . $cadena2;
        
        //Recorriendo con for y foreach los valores de las variables y colocandolas con saltos de linea entre letras
        for($c=0;$c<strlen($cadena);$c++){
            echo "$cadena[$c]<br>";
        }
        
        $vArray=str_split($cadena);
        foreach($vArray as $value){
            echo "$value<br>";
        }
        ?>
    </body>
</html>
