<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        //definiendo variables y dandoles un valor
        $entero=10;
        $cadena="hola";
        $real=23.6;
        $logico=TRUE;
        
        //mostrando el valor de las variables
        var_dump($entero);
        var_dump($cadena);
        var_dump($real);
        var_dump($logico);
        
        //convirtiendo a entero
        $logico=(int)$logico;
        //convirtiendo a float
        $entero=(float)$entero;
        settype($logico, "int");
        
        //mostrando el valor de las variables despues de haber hecho la conversion a entero y float
        var_dump($entero);
        var_dump($cadena);
        var_dump($real);
        var_dump($logico);
        
        ?>
    </body>
</html>
