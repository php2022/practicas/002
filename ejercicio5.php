<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        //inicializando variables y asignandoles valor, en este caso nombres
        $alumno1="Ramon";
        $alumno2="Jose";
        $alumno3="Pepe";
        $alumno4="Ana";
        
        //mostrando los valores de las variables arriba inicializadas
        //alumno1 y alumno 2 salen en la misma linea porque no tienen salto de linea entre ellos
        //alumno3 y alumno4 estan separados por un alto de linea
        echo $alumno1;
        echo $alumno2;
        echo "<br>";
        echo "$alumno3";
        echo "<br>";
        echo $alumno4;     
        ?>
        <div>
            <?php
                //mostrando los valores de las variables metidas entro de un div y con saltos de linea
                echo "$alumno1<br>$alumno2<br>$alumno3<br>$alumno4";
            ?>
        </div>
    </body>
</html>
